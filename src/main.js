import 'bootstrap/dist/css/bootstrap.css'
import { createApp } from 'vue'
//import { createPinia } from "pinia/dist/pinia";
import { createRouter, createWebHashHistory } from 'vue-router'
import App from './App.vue'
import './assets/main.css'
import Tasks from "./components/Tasks.vue";
import Login from "./components/Login.vue";

const routes = [
    { path: "/", component : Login},
    { path: "/dashboard", component: Tasks}
]

const router = createRouter({
    history: createWebHashHistory(),
    routes: routes
})

const app = createApp(App);
app.use(router)
//app.use(createPinia)
app.mount("#app")
import 'bootstrap/dist/js/bootstrap.js'
